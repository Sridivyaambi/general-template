{
	"AWSTemplateFormatVersion" : "2010-09-09",

	"Description" : "Creates a single VPC with two public and two private subnets and an IPsec VPN connection to the Customer Network",

	"Parameters" : {
	   "VPCCIDR" : {
            "Description" : "Enter the CIDR block of the VPC",
            "Type"        : "String",
            "Default"     : "10.67.0.0/16",
            "AllowedPattern" : "[a-zA-Z0-9]+\\..+"
        },
        "VPCName" : {
            "Description" : "Enter the name of the VPC",
            "Default"     : "DCPVPC01",
            "Type"        : "String"
        },
		"VPCDescription" : {
            "Description" : "Enter the description of the VPC",
            "Default"     : "Cloud Platforms Sydney DCPVPC01",
            "Type"        : "String"
        },
        "VPCAZs"  : {
            "Description" : "Enter the name of two VPC AZs in the same region, no more than two",
            "Default"     : "ap-southeast-2a, ap-southeast-2b",
            "Type"        : "CommaDelimitedList"
        },
        "IGWName" : {
            "Description" : "Enter the name of the Internet Gateway",
            "Default"     : "DCPVPC01_IGW01",
            "Type"        : "String"
        },
		"IGWDescription" : {
            "Description" : "Enter the description of the Internet Gateway",
            "Default"     : "Default internet gateway for DCPVPC01",
            "Type"        : "String"
        },
        "VGWName" : {
            "Description" : "Enter the name of the Virtual Gateway",
            "Default"     : "DCPVPC01_VGW01",
            "Type"        : "String"
        },
		"VGWDescription" : {
            "Description" : "Enter the description of the Virtual Gateway",
            "Default"     : "Virtual private gateway providing IPsec connectivity to Customer Network in Melbourne",
            "Type"        : "String"
        },
        "VPNName" : {
            "Description" : "Enter the name of the VPN connection",
            "Default"     : "DCPVPC01_DCPLAB_VPN01",
            "Type"        : "String"
        },
		"VPNDescription" : {
            "Description" : "Enter the description of the VPN connection",
            "Default"     : "IPsec VPN connection from DCPLAB software router",
            "Type"        : "String"
        },
        "CGWName" : {
            "Description" : "Enter the name of the Customer Gateway device",
            "Default"     : "DCPVPC01_CGW01",
            "Type"        : "String"
        },
		"CGWDescription" : {
            "Description" : "Enter the description of the Customer Gateway device",
            "Default"     : "Customer gateway for IPsec to in Melbourne",
            "Type"        : "String"
        },
        "CGWIP"   : {
            "Description" : "Enter the public IP address of the Customer Gateway device",
            "Default"     : "203.89.192.220",
            "AllowedPattern" : "[0-9]+\\..+",
            "Type"           : "String"
        },
        "CustomerFullName" : {
            "Description" : "Enter full name of the customer",
            "Default"     : "Datacom Cloud Platforms",
            "Type"        : "String"
        },
        "CustomerShortName" : {
            "Description" : "Enter short name of the customer",
            "Default"     : "DCP",
            "Type"        : "String"
        },
        "PublicRouteTableName" : {
            "Description" : "Enter the name of the public route table",
            "Default"     : "DCPVPC01_RT_PUB",
            "Type"        : "String"
        },
        "PrivateRouteTableName1" : {
            "Description" : "Enter the name of the private route table in AZa",
            "Default"     : "DCPVPC01_RT_PVT_AZ1a",
            "Type"        : "String"
        },
        "PrivateRouteTableName2" : {
            "Description" : "Enter the name of the private route table in AZb",
            "Default"     : "DCPVPC01_RT_PVT_AZ1b",
            "Type"        : "String"
        },
        "PrivateRouteTablesDefaultRoute" : {
            "Description" : "Enter the CIDR block of the customer network on the IPsec VPN",
            "Default"     : "10.0.0.0/8",
            "AllowedPattern" : "[0-9]+\\..+",
            "Type"           : "String"
        },
        "PublicSubnets"                  : {
            "Description" : "Enter the CIDR block of two public facing subnets",
            "Default"     : "10.67.0.0/24, 10.67.1.0/24",
            "Type"           : "CommaDelimitedList"
        },
        "PublicSubnetNames"              : {
            "Description" : "Enter the names of the two public subnets across AZs",
            "Default"     : "DCPVPC01_NET_DMZ_AZ1a, DCPVPC01_NET_DMZ_AZ1b",
            "Type"           : "CommaDelimitedList"
        },
        "PrivateSubnets"                 : {
            "Description" : "Enter the CIDR block of two private subnets",
            "Default"     : "10.67.4.0/24, 10.67.5.0/24",
            "Type"           : "CommaDelimitedList"
        },
        "PrivateSubnetNames"             : {
            "Description" : "Enter the names of the two private subnets across AZs",
            "Default"     : "DCPVPC01_NET_DEV_AZ1a, DCPVPC01_NET_DEV_AZ1b",
            "Type"           : "CommaDelimitedList"
        },
		"NATInstanceType" : {
			"Description" : "Instance type for NAT nodes.",
			"Type" : "String",
			"Default" : "t2.micro",
			"AllowedValues" : [ "t2.micro","m1.small","m1.medium","m1.large","m1.xlarge","m2.xlarge"],
			"ConstraintDescription" : "Must be a valid EC2 instance type."
		},
	    "KeyName" : {
			"Description" : "Name of an existing EC2 KeyPair to enable SSH access to the instances",
			"Type" : "String",
			"MinLength": "1",
			"MaxLength": "64",
			"AllowedPattern" : "[-_ a-zA-Z0-9]*",
			"ConstraintDescription" : "can contain only alphanumeric characters, spaces, dashes and underscores."
		},
		"NATNumberOfPings" : {
			"Description" : "The number of times the health check will ping the alternate NAT Node",
			"Type" : "String",
			"Default" : "3"
		},
		"NATPingTimeout" : {
			"Description" : "The number of seconds to wait for each ping response before determining that the ping has failed",
			"Type" : "String",
			"Default" : "1"
		},
		"NATWaitBetweenPings" : {
			"Description" : "The number of seconds to wait between health checks",
			"Type" : "String",
			"Default" : "2"
		},
		"NATWaitForInstanceStop" : {
			"Description" : "The number of seconds to wait for alternate NAT Node to stop before attempting to stop it again",
			"Type" : "String",
			"Default" : "60"
		},
		"NATWaitForInstanceStart" : {
			"Description" : "The number of seconds to wait for alternate NAT Node to restart before resuming health checks again",
			"Type" : "String",
			"Default" : "300"
    }
	},

	"Mappings" : {
		"AWSNATAMI" : {
		  "us-east-1"      : { "AMI" : "ami-184dc970" },
		  "us-west-2"      : { "AMI" : "ami-2dae821d" },
		  "us-west-1"      : { "AMI" : "ami-67a54423" },
		  "eu-west-1"      : { "AMI" : "ami-6975eb1e" },
		  "eu-central-1"   : { "AMI" : "ami-46073a5b" },
		  "ap-southeast-1" : { "AMI" : "ami-b49dace6" },
		  "ap-southeast-2" : { "AMI" : "ami-f2210191" },
		  "ap-northeast-1" : { "AMI" : "ami-11dc2a11" },
		  "sa-east-1"      : { "AMI" : "ami-93fb408e" }
    }
  },

	"Resources" : {
	        "VPC" : {
            "Type" : "AWS::EC2::VPC",
            "Properties" : {
                "CidrBlock" : {
                    "Ref" : "VPCCIDR"
                },
                "EnableDnsSupport" : "true",
                "EnableDnsHostnames" : "false",
                "InstanceTenancy"    : "default",
                "Tags"               : [
					{ "Key" : "Name", "Value" : { "Ref" : "VPCName" } },
					{ "Key" : "Description", "Value" : { "Ref" : "VPCDescription" } },
					{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" } }
				]
            }
        },
        "IGW" : {
            "Type" : "AWS::EC2::InternetGateway",
            "Properties" : {
                "Tags" : [
                    { "Key" : "Name", "Value" : { "Ref" : "IGWName" } },
					{ "Key" : "Description", "Value" : { "Ref" : "IGWDescription" } },
                    { "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" } }
                ]
            }
        },
        "VGW" : {
            "Type" : "AWS::EC2::VPNGateway",
            "Properties" : {
                "Type" : "ipsec.1",
                "Tags" : [
                    {
                        "Key" : "Name",
                        "Value" : {
                            "Ref" : "VGWName"
                        }
                    },
                    {
                        "Key" : "Customer",
                        "Value" : {
                            "Ref" : "CustomerFullName"
                        }
                    }
                ]
            }
        },
        "CGW" : {
            "Type" : "AWS::EC2::CustomerGateway",
            "Properties" : {
                "Type" : "ipsec.1",
                "BgpAsn" : "65000",
                "IpAddress" : {
                    "Ref" : "CGWIP"
                },
                "Tags"      : [
                    {
                        "Key" : "Name",
                        "Value" : {
                            "Ref" : "CGWName"
                        }
                    },
                    {
                        "Key" : "Customer",
                        "Value" : {
                            "Ref" : "CustomerFullName"
                        }
                    }
                ]
            }
        },
        "AttachIGW" : {
			"DependsOn" : ["VPC", "IGW"],
            "Type" : "AWS::EC2::VPCGatewayAttachment",
            "Properties" : {
                "VpcId" : {
                    "Ref" : "VPC"
                },
                "InternetGatewayId" : {
                    "Ref" : "IGW"
                }
            }
        },
        "AttachVGW" : {
			"DependsOn" : ["VPC", "VGW"],
            "Type" : "AWS::EC2::VPCGatewayAttachment",
            "Properties" : {
                "VpcId" : {
                    "Ref" : "VPC"
                },
                "VpnGatewayId" : {
                    "Ref" : "VGW"
                }
            }
        },
		"CreateVPN" : {
		"DependsOn" : ["AttachVGW", "CGW"],
			"Type" : "AWS::EC2::VPNConnection",
			"Properties" : {
				"Type" : "ipsec.1",
				"StaticRoutesOnly" : "true",
				"CustomerGatewayId" : { "Ref" : "CGW" },
				"VpnGatewayId" : { "Ref" : "VGW" },
				"Tags" : [ 
				{ "Key" : "Name", "Value" : { "Ref" : "VPNName" } },
				{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" } },
				{ "Key" : "Region", "Value" : "ap-southeast-2" }
				]			
			}
		},
		"CreateVPNConnectionRoute1" : {
		"DependsOn" : ["CreateVPN"],
		"Type" : "AWS::EC2::VPNConnectionRoute",
		"Properties" : {
			"DestinationCidrBlock" : "10.64.29.0/24",
			"VpnConnectionId" : { "Ref" : "CreateVPN" }
			}
		},
		"CreateVPNConnectionRoute2" : {
		"DependsOn" : ["CreateVPN"],
		"Type" : "AWS::EC2::VPNConnectionRoute",
		"Properties" : {
			"DestinationCidrBlock" : "10.64.30.0/24",
			"VpnConnectionId" : { "Ref" : "CreateVPN" }
			}
		},
        "PublicRouteTable1" : {
			"DependsOn" : ["VPC"],
            "Type" : "AWS::EC2::RouteTable",
            "Properties" : {
                "VpcId" : {
                    "Ref" : "VPC"
                },
                "Tags"  : [
                    {
                        "Key" : "Name",
                        "Value" : {
                            "Ref" : "PublicRouteTableName"
                        }
                    },
                    {
                        "Key" : "Customer",
                        "Value" : {
                            "Ref" : "CustomerFullName"
                        }
                    }
                ]
            }
        },
        "PrivateRouteTable1AZa" : {
			"DependsOn" : ["VPC"],
            "Type" : "AWS::EC2::RouteTable",
            "Properties" : {
                "VpcId" : {
                    "Ref" : "VPC"
                },
                "Tags"  : [
                    {
                        "Key" : "Name",
                        "Value" : {
                            "Ref" : "PrivateRouteTableName1"
                        }
                    },
                    {
                        "Key" : "Customer",
                        "Value" : {
                            "Ref" : "CustomerFullName"
                        }
                    }
                ]
            }
        },
        "PrivateRouteTable1AZb" : {
			"DependsOn" : ["VPC"],
            "Type" : "AWS::EC2::RouteTable",
            "Properties" : {
                "VpcId" : {
                    "Ref" : "VPC"
                },
                "Tags"  : [
                    {
                        "Key" : "Name",
                        "Value" : {
                            "Ref" : "PrivateRouteTableName2"
                        }
                    },
                    {
                        "Key" : "Customer",
                        "Value" : {
                            "Ref" : "CustomerFullName"
                        }
                    }
                ]
            }
        },
        "PublicRouteTable1Route1" : {
			"DependsOn" : ["PublicRouteTable1", "IGW"],
            "Type" : "AWS::EC2::Route",
            "Properties" : {
                "RouteTableId" : {
                    "Ref" : "PublicRouteTable1"
                },
                "DestinationCidrBlock" : "0.0.0.0/0",
                "GatewayId"            : {
                    "Ref" : "IGW"
                }
            }
        },
        "PrivateRouteTable1AZaDefaultRoute" : {
			"DependsOn" : ["PrivateRouteTable1AZa", "AttachVGW"],
            "Type" : "AWS::EC2::Route",
            "Properties" : {
                "RouteTableId" : {
                    "Ref" : "PrivateRouteTable1AZa"
                },
                "DestinationCidrBlock" : {
                    "Ref" : "PrivateRouteTablesDefaultRoute"
                },
                "GatewayId"            : {
                    "Ref" : "VGW"
                }
            }
        },
        "PrivateRouteTable1AZbDefaultRoute" : {
			"DependsOn" : ["PrivateRouteTable1AZb", "AttachVGW"],
            "Type" : "AWS::EC2::Route",
            "Properties" : {
                "RouteTableId" : {
                    "Ref" : "PrivateRouteTable1AZb"
                },
                "DestinationCidrBlock" : {
                    "Ref" : "PrivateRouteTablesDefaultRoute"
                },
                "GatewayId"            : {
                    "Ref" : "VGW"
                }
            }
        },
        "PublicSubnet1"                     : {
			"DependsOn" : ["VPC"],
            "Type" : "AWS::EC2::Subnet",
            "Properties" : {
                "AvailabilityZone" : {
                    "Fn::Select" : [
                        "0",
                        {
                            "Ref" : "VPCAZs"
                        }
                    ]
                },
                "VpcId"            : {
                    "Ref" : "VPC"
                },
                "CidrBlock"        : {
                    "Fn::Select" : [
                        "0",
                        {
                            "Ref" : "PublicSubnets"
                        }
                    ]
                },
				"Tags" : [ { "Key" : "Name", "Value" : { "Fn::Select" : [ 0, { "Ref" : "PublicSubnetNames"} ] } },
				{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" }}
				]
            }
        },
        "PublicSubnet2"                     : {
			"DependsOn" : ["VPC"],
            "Type" : "AWS::EC2::Subnet",
            "Properties" : {
                "AvailabilityZone" : {
                    "Fn::Select" : [
                        "1",
                        {
                            "Ref" : "VPCAZs"
                        }
                    ]
                },
                "VpcId"            : {
                    "Ref" : "VPC"
                },
                "CidrBlock"        : {
                    "Fn::Select" : [
                        "1",
                        {
                            "Ref" : "PublicSubnets"
                        }
                    ]
                },
				"Tags" : [ 
				{ "Key" : "Name", "Value" : { "Fn::Select" : [ 1, { "Ref" : "PublicSubnetNames"} ] } },
				{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" }}
				]
            }
        },
        "PrivateSubnet1"                    : {
			"DependsOn" : ["VPC"],
            "Type" : "AWS::EC2::Subnet",
            "Properties" : {
                "AvailabilityZone" : {
                    "Fn::Select" : [
                        "0",
                        {
                            "Ref" : "VPCAZs"
                        }
                    ]
                },
                "VpcId"            : {
                    "Ref" : "VPC"
                },
                "CidrBlock"        : {
                    "Fn::Select" : [
                        "0",
                        {
                            "Ref" : "PrivateSubnets"
                        }
                    ]
                },
				"Tags" : [ { "Key" : "Name", "Value" : { "Fn::Select" : [ 0, { "Ref" : "PrivateSubnetNames"} ] } },
				{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" }}
				]
            }
        },
        "PrivateSubnet2"                    : {
			"DependsOn" : ["VPC"],
            "Type" : "AWS::EC2::Subnet",
            "Properties" : {
                "AvailabilityZone" : {
                    "Fn::Select" : [
                        "1",
                        {
                            "Ref" : "VPCAZs"
                        }
                    ]
                },
                "VpcId"            : {
                    "Ref" : "VPC"
                },
                "CidrBlock"        : {
                    "Fn::Select" : [
                        "1",
                        {
                            "Ref" : "PrivateSubnets"
                        }
                    ]
                },
				"Tags" : [ { "Key" : "Name", "Value" : { "Fn::Select" : [ 1, { "Ref" : "PrivateSubnetNames"} ] } },
				{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" }}
				]
            }
        },
		"PublicSubnet1RouteTableAssociation"                    : {
			"DependsOn" : ["VPC"],
            "Type" : "AWS::EC2::SubnetRouteTableAssociation",
            "Properties" : {
                "SubnetId" : { "Ref" : "PublicSubnet1" },
                "RouteTableId" : { "Ref" : "PublicRouteTable1" }               
            }
        },
		"PublicSubnet2RouteTableAssociation"                    : {
			"DependsOn" : ["VPC"],
            "Type" : "AWS::EC2::SubnetRouteTableAssociation",
            "Properties" : {
                "SubnetId" : { "Ref" : "PublicSubnet2" },
                "RouteTableId" : { "Ref" : "PublicRouteTable1" }               
            }
        },
		"PrivateSubnet1RouteTableAssociation"                    : {
			"DependsOn" : ["VPC"],
            "Type" : "AWS::EC2::SubnetRouteTableAssociation",
            "Properties" : {
                "SubnetId" : { "Ref" : "PrivateSubnet1" },
                "RouteTableId" : { "Ref" : "PrivateRouteTable1AZa" }               
            }
        },
		"PrivateSubnet2RouteTableAssociation"                    : {
			"DependsOn" : ["VPC"],
            "Type" : "AWS::EC2::SubnetRouteTableAssociation",
            "Properties" : {
                "SubnetId" : { "Ref" : "PrivateSubnet2" },
                "RouteTableId" : { "Ref" : "PrivateRouteTable1AZb" }               
            }
        },
		"GeneralConnectivitySecurityGroup" : {
		   "Type" : "AWS::EC2::SecurityGroup",
		   "Properties" : {			
			  "GroupDescription" : "Allow pings",
			  "VpcId" : {"Ref" : "VPC"},
			  "SecurityGroupIngress" : [
			  { "IpProtocol" : "ICMP", "FromPort" : "8", "ToPort" : "-1", "CidrIp" : { "Ref" : "VPCCIDR" }},
			  { "IpProtocol" : "ICMP", "FromPort" : "8", "ToPort" : "-1", "CidrIp" : { "Ref" : "PrivateRouteTablesDefaultRoute" }}
			  ],
			  "Tags" : [
				{ "Key" : "Name", "Value" : "SG-GeneralConnectivity"},
				{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" }}
			  ]
		   }
		},
		"LinuxConnectivitySecurityGroup" : {
		   "Type" : "AWS::EC2::SecurityGroup",
		   "Properties" : {
			  "GroupDescription" : "SSH access from the VPN",
			  "VpcId" : {"Ref" : "VPC"},
			  "SecurityGroupIngress" : [
			  { "IpProtocol" : "TCP", "FromPort" : "22", "ToPort" : "22", "CidrIp" : { "Ref" : "PrivateRouteTablesDefaultRoute" }}
			  ],
			  "Tags" : [
				{ "Key" : "Name", "Value" : "SG-LinuxConnectivity"},
				{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" }}
			  ]
		   }
		},
		"WindowsConnectivitySecurityGroup" : {
		   "Type" : "AWS::EC2::SecurityGroup",
		   "Properties" : {
			  "GroupDescription" : "General Windows connectivity from the VPN",
			  "VpcId" : {"Ref" : "VPC"},
			  "SecurityGroupIngress" : [
			  { "IpProtocol" : "TCP", "FromPort" : "3389", "ToPort" : "3389", "CidrIp" : { "Ref" : "PrivateRouteTablesDefaultRoute" }},
			  { "IpProtocol" : "TCP", "FromPort" : "49152", "ToPort" : "65535", "CidrIp" : { "Ref" : "PrivateRouteTablesDefaultRoute" }}
			  ],
			  "Tags" : [
				{ "Key" : "Name", "Value" : "SG-WindowsConnectivity"},
				{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" }}
			  ]
		   }
		},
		"NATSecurityGroup" : {
		  "Type" : "AWS::EC2::SecurityGroup",
		  "Properties" : {
			"GroupDescription" : "Rules for allowing access to HA Nodes",
			"VpcId" : { "Ref" : "VPC" },
			"SecurityGroupIngress" : [
			   { "IpProtocol" : "tcp", "FromPort" : "22",  "ToPort" : "22",  "CidrIp" : "10.0.0.0/8" } ,
			   { "IpProtocol" : "icmp", "FromPort" : "8",  "ToPort" : "-1",  "CidrIp" : "0.0.0.0/0"} ],
			"SecurityGroupEgress" : [
			   { "IpProtocol" : "-1", "FromPort" : "0", "ToPort" : "65535", "CidrIp" : "0.0.0.0/0" } ],
			 "Tags" : [
				{ "Key" : "Name", "Value" : "SG-NAT"},
				{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" }}
			  ]
      }
    },
		"NATRole": {
		   "Type": "AWS::IAM::Role",
		   "Properties": {
			  "AssumeRolePolicyDocument": {
				 "Statement": [ {
					"Effect": "Allow",
					"Principal": {
					   "Service": [ "ec2.amazonaws.com" ]
					},
					"Action": [ "sts:AssumeRole" ]
				 } ]
			  },
			  "Path": "/",
			  "Policies": [ {
				 "PolicyName": "ManagedPolicyNATTakeover",
				 "PolicyDocument": {
					"Statement": [ {
					   "Effect": "Allow",
					   "Action": [
							"ec2:DescribeInstances",
							"ec2:DescribeRouteTables",
							"ec2:CreateRoute",
							"ec2:ReplaceRoute",
							"ec2:StartInstances",
							"ec2:StopInstances"
					   ],
					   "Resource": "*"
					} ]
				 }
				 } ]
			  }
    },
	    "NATRoleProfile": {
       "Type": "AWS::IAM::InstanceProfile",
       "Properties": {
          "Path": "/",
          "Roles": [ {
             "Ref": "NATRole"
          } ]
       }
    },
		"NAT1EIP" : {
      "Type" : "AWS::EC2::EIP",
      "Properties" : {
        "Domain" : "vpc",
        "InstanceId" : { "Ref" : "NAT1Instance" }
      }
    },
		"NAT2EIP" : {
      "Type" : "AWS::EC2::EIP",
      "Properties" : {
        "Domain" : "vpc",
        "InstanceId" : { "Ref" : "NAT2Instance" }
      }
    },
		"NAT1Instance" : {
      "Type" : "AWS::EC2::Instance",
      "Metadata" : {
        "Comment1" : "Create NAT #1"
      },
      "Properties" : {
        "InstanceType" : { "Ref" : "NATInstanceType" } ,
       		"IamInstanceProfile" : { "Ref" : "NATRoleProfile" },
        "SubnetId" : { "Ref" : "PublicSubnet1" },
        "SourceDestCheck" : "false",
        "ImageId" : { "Fn::FindInMap" : [ "AWSNATAMI", { "Ref" : "AWS::Region" }, "AMI" ]},
        "SecurityGroupIds" : [{ "Ref" : "NATSecurityGroup" }],
        "Tags" : [
				{ "Key" : "Name", "Value" : "NAT01a"},
				{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" }}
			  ],
        "UserData"       : { "Fn::Base64" : { "Fn::Join" : ["", [
			"#!/bin/bash -v\n",
			"yum update -y aws*\n",
			". /etc/profile.d/aws-apitools-common.sh\n",
			"# Configure iptables\n",
			"/sbin/iptables -t nat -A POSTROUTING -o eth0 -s 0.0.0.0/0 -j MASQUERADE\n",
			"/sbin/iptables-save > /etc/sysconfig/iptables\n",
			"# Configure ip forwarding and redirects\n",
			"echo 1 >  /proc/sys/net/ipv4/ip_forward && echo 0 >  /proc/sys/net/ipv4/conf/eth0/send_redirects\n",
			"mkdir -p /etc/sysctl.d/\n",
			"cat <<EOF > /etc/sysctl.d/nat.conf\n",
			"net.ipv4.ip_forward = 1\n",
			"net.ipv4.conf.eth0.send_redirects = 0\n",
			"EOF\n",
			"# Download nat_monitor.sh and configure\n",
			"cd /root\n",
			"wget http://media.amazonwebservices.com/articles/nat_monitor_files/nat_monitor.sh\n",
			"NAT_ID=\n",
			"# CloudFormation should have updated the PrivateRouteTable1AZa by now (due to yum update), however loop to make sure\n",
			"while [ \"$NAT_ID\" == \"\" ]; do\n",
			"  sleep 60\n",
			"  NAT_ID=`/opt/aws/bin/ec2-describe-route-tables ", { "Ref" : "PrivateRouteTable1AZa" }, 
				" -U https://ec2.", { "Ref" : "AWS::Region" }, ".amazonaws.com | grep 0.0.0.0/0 | awk '{print $2;}'`\n",
			"  #echo `date` \"-- NAT_ID=$NAT_ID\" >> /tmp/test.log\n",
			"done\n",
			"# Update NAT_ID, NAT_RT_ID, and My_RT_ID\n",
			"sed \"s/NAT_ID=/NAT_ID=$NAT_ID/g\" /root/nat_monitor.sh > /root/nat_monitor.tmp\n",
			"sed \"s/NAT_RT_ID=/NAT_RT_ID=",
			{ "Ref" : "PrivateRouteTable1AZb" },
			"/g\" /root/nat_monitor.tmp > /root/nat_monitor.sh\n",
			"sed \"s/My_RT_ID=/My_RT_ID=",
			{ "Ref" : "PrivateRouteTable1AZa" },
			"/g\" /root/nat_monitor.sh > /root/nat_monitor.tmp\n",
			"sed \"s/EC2_URL=/EC2_URL=https:\\/\\/ec2.",
			{ "Ref" : "AWS::Region" }, ".amazonaws.com",
			"/g\" /root/nat_monitor.tmp > /root/nat_monitor.sh\n",
			"sed \"s/Num_Pings=3/Num_Pings=",
			{ "Ref" : "NATNumberOfPings" },
			"/g\" /root/nat_monitor.sh > /root/nat_monitor.tmp\n",
			"sed \"s/Ping_Timeout=1/Ping_Timeout=",
			{ "Ref" : "NATPingTimeout" },
			"/g\" /root/nat_monitor.tmp > /root/nat_monitor.sh\n",
			"sed \"s/Wait_Between_Pings=2/Wait_Between_Pings=",
			{ "Ref" : "NATWaitBetweenPings" },
			"/g\" /root/nat_monitor.sh > /root/nat_monitor.tmp\n",
			"sed \"s/Wait_for_Instance_Stop=60/Wait_for_Instance_Stop=",
			{ "Ref" : "NATWaitForInstanceStop" },
			"/g\" /root/nat_monitor.tmp > /root/nat_monitor.sh\n",
			"sed \"s/Wait_for_Instance_Start=300/Wait_for_Instance_Start=",
			{ "Ref" : "NATWaitForInstanceStart" },
			"/g\" /root/nat_monitor.sh > /root/nat_monitor.tmp\n",
			"mv /root/nat_monitor.tmp /root/nat_monitor.sh\n",
			"chmod a+x /root/nat_monitor.sh\n",
			"echo '@reboot /root/nat_monitor.sh > /tmp/nat_monitor.log' | crontab\n",
			"/root/nat_monitor.sh > /tmp/nat_monitor.log &\n"
        ]]}}
      }
    },
		"NAT2Instance" : {
      "Type" : "AWS::EC2::Instance",
      "Metadata" : {
        "Comment1" : "Create NAT #2"
      },
      "Properties" : {
        "InstanceType" : { "Ref" : "NATInstanceType" } ,
       		"IamInstanceProfile" : { "Ref" : "NATRoleProfile" },
        "SubnetId" : { "Ref" : "PublicSubnet2" },
        "SourceDestCheck" : "false",
        "ImageId" : { "Fn::FindInMap" : [ "AWSNATAMI", { "Ref" : "AWS::Region" }, "AMI" ]},
        "SecurityGroupIds" : [{ "Ref" : "NATSecurityGroup" }],
        "Tags" : [
				{ "Key" : "Name", "Value" : "NAT01b"},
				{ "Key" : "Customer", "Value" : { "Ref" : "CustomerFullName" }}
			  ],
        "UserData"       : { "Fn::Base64" : { "Fn::Join" : ["", [
			"#!/bin/bash -v\n",
			"yum update -y aws*\n",
			"# Configure iptables\n",
			"/sbin/iptables -t nat -A POSTROUTING -o eth0 -s 0.0.0.0/0 -j MASQUERADE\n",
			"/sbin/iptables-save > /etc/sysconfig/iptables\n",
			"# Configure ip forwarding and redirects\n",
			"echo 1 >  /proc/sys/net/ipv4/ip_forward && echo 0 >  /proc/sys/net/ipv4/conf/eth0/send_redirects\n",
			"mkdir -p /etc/sysctl.d/\n",
			"cat <<EOF > /etc/sysctl.d/nat.conf\n",
			"net.ipv4.ip_forward = 1\n",
			"net.ipv4.conf.eth0.send_redirects = 0\n",
			"EOF\n",
			"# Download nat_monitor.sh and configure\n",
			"cd /root\n",
			"wget http://media.amazonwebservices.com/articles/nat_monitor_files/nat_monitor.sh\n",
			"# Update NAT_ID, NAT_RT_ID, and My_RT_ID\n",
			"sed \"s/NAT_ID=/NAT_ID=",
			{ "Ref" : "NAT1Instance" },
			"/g\" /root/nat_monitor.sh > /root/nat_monitor.tmp\n",
			"sed \"s/NAT_RT_ID=/NAT_RT_ID=",
			{ "Ref" : "PrivateRouteTable1AZa" },
			"/g\" /root/nat_monitor.tmp > /root/nat_monitor.sh\n",
			"sed \"s/My_RT_ID=/My_RT_ID=",
			{ "Ref" : "PrivateRouteTable1AZb" },
			"/g\" /root/nat_monitor.sh > /root/nat_monitor.tmp\n",
			"sed \"s/EC2_URL=/EC2_URL=https:\\/\\/ec2.",
			{ "Ref" : "AWS::Region" }, ".amazonaws.com",
			"/g\" /root/nat_monitor.tmp > /root/nat_monitor.sh\n",
			"sed \"s/Num_Pings=3/Num_Pings=",
			{ "Ref" : "NATNumberOfPings" },
			"/g\" /root/nat_monitor.sh > /root/nat_monitor.tmp\n",
			"sed \"s/Ping_Timeout=1/Ping_Timeout=",
			{ "Ref" : "NATPingTimeout" },
			"/g\" /root/nat_monitor.tmp > /root/nat_monitor.sh\n",
			"sed \"s/Wait_Between_Pings=2/Wait_Between_Pings=",
			{ "Ref" : "NATWaitBetweenPings" },
			"/g\" /root/nat_monitor.sh > /root/nat_monitor.tmp\n",
			"sed \"s/Wait_for_Instance_Stop=60/Wait_for_Instance_Stop=",
			{ "Ref" : "NATWaitForInstanceStop" },
			"/g\" /root/nat_monitor.tmp > /root/nat_monitor.sh\n",
			"sed \"s/Wait_for_Instance_Start=300/Wait_for_Instance_Start=",
			{ "Ref" : "NATWaitForInstanceStart" },
			"/g\" /root/nat_monitor.sh > /root/nat_monitor.tmp\n",
			"mv /root/nat_monitor.tmp /root/nat_monitor.sh\n",
			"chmod a+x /root/nat_monitor.sh\n",
			"echo '@reboot /root/nat_monitor.sh > /tmp/nat_monitor.log' | crontab\n",
			"/root/nat_monitor.sh >> /tmp/nat_monitor.log &\n"
					]
				]
			}
		}
      }
    },
		"PrivateRoute1" : {
		  "Type" : "AWS::EC2::Route",
		  "Properties" : {
			"RouteTableId" : { "Ref" : "PrivateRouteTable1AZa" },
			"DestinationCidrBlock" : "0.0.0.0/0",
			"InstanceId" : { "Ref" : "NAT1Instance" }
			}
		},
		"PrivateRoute2" : {
		  "Type" : "AWS::EC2::Route",
		  "Properties" : {
			"RouteTableId" : { "Ref" : "PrivateRouteTable1AZb" },
			"DestinationCidrBlock" : "0.0.0.0/0",
			"InstanceId" : { "Ref" : "NAT2Instance" }
			}
		}	    
	},

	"Outputs" : {
	}
}

